package body nodoublefork is
   protected body fork is
      entry seize when is_available is
      begin
         is_available := false;
      end seize;

      procedure release is
      begin
         is_available := true;
      end release;

   end fork;

end nodoublefork;
