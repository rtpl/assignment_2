package body deadlock_prevention is
   protected body steve is
      entry getPermission when allowed_to_eat is
      begin
         number_of_eating_phils := number_of_eating_phils + 1;
         if number_of_eating_phils > 3 then
            allowed_to_eat := false;
         end if;
      end getPermission;

      procedure finishedEating is
      begin
         number_of_eating_phils := number_of_eating_phils - 1;
         allowed_to_eat := true;
      end finishedEating;

   end steve;
end deadlock_prevention;
