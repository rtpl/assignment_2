with phil_settings;

package deadlock_prevention is
   protected type steve is
      entry getPermission;
      procedure finishedEating;
   private
      allowed_to_eat: Boolean := true;
      number_of_eating_phils: phil_settings.Philosophers_Range := 0;
   end steve;

   oneAndOnlySteve: steve;
end deadlock_prevention;
