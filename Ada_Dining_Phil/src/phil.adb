-- pragma Profile(Ravenscar); -- using local pragma in project settings
with Ada.Text_IO, Ada.Real_Time, phil_settings, nodoublefork, deadlock_prevention;
use Ada.Text_IO, Ada.Real_Time, phil_settings, nodoublefork, deadlock_prevention;

package body phil is
   task body philtask is
      Next_Hunger : Time ;
      Period : constant Time_Span := THINKTIME;
      id : constant Philosophers_Range := discr_id;
      fork_right : constant Philosophers_Range := discr_id;
      fork_left : Philosophers_Range;
   begin
      Next_Hunger := Clock;

      if fork_right = Philosophers_Range'Last then
         fork_left := Philosophers_Range'First;
      else
         fork_left := fork_right + 1;
      end if;

      loop
         delay until Next_Hunger;
         Put_Line("P" & id'img & " Waiting");
         -- ## get forks
         oneAndOnlySteve.getPermission;
         nodoublefork.forks(fork_right).seize;
         nodoublefork.forks(fork_left).seize;

         -- ## do some eating
         Put_Line("P" & id'img & " Eating");

         -- Added eating time to see some output
         delay until Clock + Milliseconds(1500);

         -- ## release forks
         nodoublefork.forks(fork_right).release;
         nodoublefork.forks(fork_left).release;
         oneAndOnlySteve.finishedEating;

         Put_Line("P" & id'img & " Thinking");
         Next_Hunger := Next_Hunger + Period ;
      end loop;
   end philtask;

   p0: philtask(0);
   p1: philtask(1);
   p2: philtask(2);
   p3: philtask(3);
   p4: philtask(4);


end phil;
