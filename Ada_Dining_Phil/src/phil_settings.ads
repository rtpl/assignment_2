-- pragma Profile(Ravenscar); -- using local pragma in project settings
with Ada.Real_Time;
use Ada.Real_Time;
package phil_settings is
   N : constant Integer := 5;  -- Number of Philosophers
   -- Increased thinkingtime to see some output
   THINKTIME : constant Time_Span := Milliseconds (750);
   subtype Philosophers_Range is Integer range 0 .. N-1;
end phil_settings;
