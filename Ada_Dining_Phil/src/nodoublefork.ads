with phil_settings;

package nodoublefork is
   protected type fork is
      entry seize;
      procedure release;
   private
      is_available: Boolean := true;
   end fork;

   forks: array(phil_settings.Philosophers_Range) of fork;
end nodoublefork;
